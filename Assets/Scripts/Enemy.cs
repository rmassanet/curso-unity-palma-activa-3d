﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float speed;

	private GameObject _player;
	private Transform _transform;
	private Transform _playerTransform;
	private Rigidbody _rigidbody;

	// Use this for initialization
	void Start () {
		_player = GameObject.FindGameObjectWithTag ("Player");
		_playerTransform = _player.transform;
		_transform = transform;
		_rigidbody = rigidbody;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 playerDirection = (_playerTransform.position - _transform.position).normalized;
		_rigidbody.velocity = playerDirection * speed;
	}
}
