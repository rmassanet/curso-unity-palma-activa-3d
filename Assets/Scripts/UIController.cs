﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

	public Text scoreText;

	private int _score = 0;

	// Use this for initialization
	void Start () {
		Destroyable.OnDestroyed += HandleOnDestroyed;
	}

	void OnDestroy()
	{
		Destroyable.OnDestroyed -= HandleOnDestroyed;
	}

	void HandleOnDestroyed ()
	{
		_score++;
		scoreText.text = "SCORE: " + _score.ToString();
//		scoreText.text = _score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
