﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	public GameObject bulletPrefab;
	public float bulletImpulse;

	private Transform _transform;

	// Use this for initialization
	void Start () {
		_transform = transform;

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire1"))
		{
			GameObject bullet = (GameObject)GameObject.Instantiate(bulletPrefab, _transform.position, Quaternion.identity);
			bullet.rigidbody.AddForce(_transform.forward * bulletImpulse, ForceMode.Impulse);
		}
	}
}
