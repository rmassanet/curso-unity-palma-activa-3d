﻿using UnityEngine;
using System.Collections;

public class TestNetworkPlayer : MonoBehaviour
{
	public GameObject bulletPrefab;
	public Material[] skyboxes;

	private TestNetworkUIController _uiController;
	private TestNetworkUIController _UiController
	{
		get
		{
			if(_uiController == null)
			{
				GameObject uiControllerObject = GameObject.FindGameObjectWithTag("UIController");
				if(uiControllerObject != null)
				{
					_uiController = uiControllerObject.GetComponent<TestNetworkUIController>();
				}
			}

			return _uiController;
		}
	}

	private void OnNetworkInstantiate(NetworkMessageInfo info)
	{
//		_UiController.DebugMessage("guid: " + info.sender.guid);

		if(info.sender.guid == "0")
		{
			_UiController.DebugMessage("Spawning my own player");
		}
		else
		{
			_UiController.DebugMessage("Spawning other player's player");
			Destroy (gameObject.GetComponent<MouseLook>());
			Destroy (gameObject.GetComponent<FPSInputController>());
			Destroy (gameObject.GetComponent<CharacterMotor>());
			Destroy (gameObject.GetComponent<CharacterController>());
			Camera camera = gameObject.GetComponentInChildren<Camera>();
			Destroy(camera.gameObject);
		}
	}


	private void Update()
	{
		if (networkView.isMine) {
			if (Input.GetMouseButtonDown (0)) {
				networkView.RPC ("SpawnBullet", RPCMode.All, transform.position, transform.forward * 100f);
			}

			if(Input.GetKeyDown(KeyCode.S))
			{
			}
		}
	}

	[RPC]
	private void SpawnBullet(Vector3 position, Vector3 velocity)
	{
		NetworkViewID id = Network.AllocateViewID ();
		GameObject bullet = (GameObject) GameObject.Instantiate (bulletPrefab, position, Quaternion.identity);
		bullet.rigidbody.velocity = velocity;
		NetworkView networkView = bullet.AddComponent<NetworkView> ();
		networkView.viewID = id;
	}
}
