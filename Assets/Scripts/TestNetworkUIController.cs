﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TestNetworkUIController : MonoBehaviour
{

	public Button createServerButton;
	public CanvasGroup serversPanel;
	public GameObject networkingButtonPrefab;
	public Text debugText;

	public TestNetworking networking;

	private List<HostData> _servers = new List<HostData>();

	private void OnServerInitialized()
	{
		createServerButton.interactable = false;
	}

	private void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
		{
			ClearServersPanel();

			HostData[] hostList = MasterServer.PollHostList();
			if(hostList != null && hostList.Length > 0)
			{
				SetServersPanel(true);

				if(hostList.Length > 1)
				{
					serversPanel.GetComponent<VerticalLayoutGroup>().enabled = true;
				}
				else
				{
					serversPanel.GetComponent<VerticalLayoutGroup>().enabled = false;
				}

				int idx = 0;
				foreach(HostData hostData in hostList)
				{
					_servers.Add(hostData);
					GameObject newButton = (GameObject)Instantiate(networkingButtonPrefab);
					newButton.transform.SetParent(serversPanel.transform, false);
					newButton.GetComponentInChildren<Text>().text = hostData.gameName;
					newButton.GetComponent<Button>().onClick.AddListener( () => { OnButtonClicked(idx++); } );
				}
			}
			else
			{
				SetServersPanel(false);
			}
		}
	}

	private void OnConnectedToServer()
	{
//		DebugMessage("Client connected!");
		SetServersPanel(false);
		createServerButton.interactable = false;
	}

	public void OnButtonClicked(int buttonIndex)
	{
		HostData hostData = _servers[buttonIndex];
		networking.JoinServer(hostData);
	}

	private void ClearServersPanel()
	{
		foreach(Transform child in serversPanel.transform)
		{
			Destroy(child.gameObject);
		}
	}

	public void SetServersPanel(bool active)
	{
		serversPanel.alpha = active ? 1f : 0f;
		serversPanel.interactable = active;
		serversPanel.blocksRaycasts = active;
	}

	public void DebugMessage(string message)
	{
		debugText.text = message;
	}
}
