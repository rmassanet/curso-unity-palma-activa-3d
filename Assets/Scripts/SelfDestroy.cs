﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {

	public float delay;

	void Start () {
		Invoke ("DestroyObject", delay);
	}

	void DestroyObject()
	{
		Destroy (gameObject);
	}
}
