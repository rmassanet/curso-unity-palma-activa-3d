﻿using UnityEngine;
using System.Collections;

public class TestNetworking : MonoBehaviour
{
	private const string gameIdentifier = "TutorialNetworkingPalmaActiva"; // Identificador unico!
	public string gameName;
	
	public void StartServer()
	{
		Network.InitializeServer(10, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(gameIdentifier, gameName);
	}

	public void RefreshHostList()
	{
		MasterServer.RequestHostList(gameIdentifier);
	}

	public void JoinServer(HostData hostData)
	{
		Debug.Log("Joining server: " + hostData.gameName);
		Network.Connect(hostData);
	}
}
