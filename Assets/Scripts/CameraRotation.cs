﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {

	public Transform target;
	public float distanceToTarget;
	public float altitude;

	private Transform _transform;
	private Rigidbody _rigidbody;

	// Use this for initialization
	void Start () {
		_transform = transform;
		_rigidbody = target.rigidbody;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 movementBall = _rigidbody.velocity;
		Vector3 direction = -movementBall.normalized;
//		Vector3 targetToCam = (_transform.position - target.position).normalized;
		Vector3 targetPoint = target.position + distanceToTarget * direction;
		targetPoint.y += altitude;

		_transform.position = targetPoint;
		_transform.LookAt (target.position);
	}
}
