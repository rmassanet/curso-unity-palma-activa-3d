﻿using UnityEngine;
using System.Collections;

public class Destroyed : MonoBehaviour {

	public float expansionForce;
	public float torque;

	// Use this for initialization
	void Start () {
		DoDestroy ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DoDestroy()
	{
		foreach (Transform child in transform) {
			Vector3 expansionDirection = child.localPosition.normalized;
			child.rigidbody.AddForce(expansionDirection * expansionForce, ForceMode.Impulse);
			child.rigidbody.AddTorque(expansionDirection * torque);
		}

//		for (int i = 0; i < transform.childCount; i++) {
//			transform.GetChild(i);
//		}
	}
}
