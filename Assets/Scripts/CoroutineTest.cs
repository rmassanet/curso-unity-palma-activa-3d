﻿using UnityEngine;
using System.Collections;

public class CoroutineTest : MonoBehaviour {

	public Material material;

	void Start () {
//		Debug.Log ("Antes de la corutina");
		StartCoroutine(ChangeColors ());
//		Debug.Log ("Despues de la corutina");
	}

	IEnumerator ChangeColors()
	{
		material.SetColor ("_Color", Color.red);
		yield return new WaitForSeconds(3f);
//		Debug.Log ("Dentro de la corutina");
		material.SetColor ("_Color", Color.green);
		yield return new WaitForSeconds(1f);
		material.SetColor ("_Color", Color.blue);
		
		
	}
}
