﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveForce;
	public float jumpForce;

	private Rigidbody _rigidbody;
	private bool _jump;

	// Use this for initialization
	void Start () {
		_rigidbody = rigidbody;
	}
	
	// Update is called once per frame
	void Update () {
//		if (!_jump) {
			_jump = Input.GetButtonDown ("Jump");
//		}
	}

	void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");

		_rigidbody.AddForce (new Vector3 (h, 0, v) * moveForce);

		if (_jump) {
			_rigidbody.AddForce (new Vector3 (0, jumpForce, 0));
			_jump = false;
		}
	}
}
