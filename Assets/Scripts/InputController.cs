﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
	public float speed;
	public Rigidbody target;

	private Camera _camera;
	private Transform _transform;
	private Vector3 _targetPosition;
	private bool _isGoingToTarget = false;

	// Use this for initialization
	void Start () {
		_camera = camera;
		_transform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		// Raycast
		Vector3 mousePosition = Input.mousePosition;
		Ray worldRay = camera.ScreenPointToRay (mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (worldRay, out hit, 1000f)) {
			if(hit.collider.gameObject.tag == "Terrain")
			{
				_targetPosition = hit.point;
				_isGoingToTarget = true;
			}
		}

		// Update position
		if (_isGoingToTarget) {
			Vector3 direction = (_targetPosition - target.transform.position).normalized;
			target.velocity = direction * speed;
		}
	}
}
