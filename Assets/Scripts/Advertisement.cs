﻿using UnityEngine;
using System.Collections;

public class Advertisement : MonoBehaviour {

	public delegate void DownloadHandler (Texture2D texture);
	public static event DownloadHandler OnTextureReady;

	public string[] url;
	public Material adMaterial;

	private int _current = 0;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("ChangeAdvertisement", 0, 10f);
	}
	
	void ChangeAdvertisement()
	{
		_current = (_current + 1) % url.Length;
		StartCoroutine (DownloadTextureWithEvent (url[_current]));
	}

	IEnumerator DownloadTexture(string url, DownloadHandler Callback)
	{
		WWW www = new WWW (url);
		while (!www.isDone) {
			Debug.Log(www.progress);
			yield return new WaitForSeconds(0.1f);
		}

		Texture2D texture = www.texture;
		Callback (texture);
	}

	IEnumerator DownloadTextureWithEvent(string url)
	{
		WWW www = new WWW (url);
		while (!www.isDone) {
			Debug.Log(www.progress);
			yield return new WaitForSeconds(0.1f);
		}
		
		Texture2D texture = www.texture;

		if (OnTextureReady != null) {
			OnTextureReady (texture);
		}
	}

	void TextureHandler (Texture2D texture)
	{
		Debug.Log ("The texture is ready");
		adMaterial.SetTexture ("_MainTex", texture);
	}
}
