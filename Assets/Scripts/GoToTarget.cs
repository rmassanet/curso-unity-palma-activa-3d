﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class GoToTarget : MonoBehaviour
{
	public Transform target;

	private NavMeshAgent _agent;

	// Use this for initialization
	void Start () {
		_agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		_agent.SetDestination (target.position);
	}
}
