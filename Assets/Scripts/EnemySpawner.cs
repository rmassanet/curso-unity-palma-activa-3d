﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;

public class EnemySpawner : MonoBehaviour
{
	public GameObject enemyPrefab;
	public Transform[] spawnPoints;

	public float waveTime;
	public int[] waves;
	public int[] enemies;
	public float[] enemyStrength;
	public float[] enemySpeed;

	private int _currentWave;
	private float _timeLastWave;
	private int _enemiesAlive;
	private bool _spawning;

	// Use this for initialization
	void Start () {
		Destroyable.OnDestroyed += HandleOnDestroyed;
		_currentWave = 0;
		_spawning = true;
		LaunchWave ();

		Debug.Log (Serialize ());
	}

	void HandleOnDestroyed ()
	{
		_enemiesAlive--;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!_spawning) {
			return;
		}

		if (_enemiesAlive == 0) {
			_currentWave++;

			if(_currentWave == waves.Length)
			{
				LevelFinished();
			}
			else
			{
				LaunchWave();
			}
		}

		float now = Time.time;
		if (now - _timeLastWave > waveTime)
		{
			_currentWave++;
			if(_currentWave == waves.Length)
			{
				LevelFinished();
			}
			else
			{
				LaunchWave();
			}
		}
	}

	void LevelFinished()
	{
		Debug.Log ("Level finished!");
		_spawning = false;
	}

	void LaunchWave()
	{
		List<int> usedSpawnPoints = new List<int> ();

		int numEnemies = enemies [_currentWave];
		for (int i = 0; i < numEnemies; i++) {
			int rand;
			do
			{
				rand = Random.Range(0, spawnPoints.Length);
			}
			while(usedSpawnPoints.Contains(rand));

			usedSpawnPoints.Add(rand);

			GameObject enemyObject = (GameObject) GameObject.Instantiate(
				enemyPrefab,
				spawnPoints[rand].position,
				Quaternion.identity
			);
			Enemy enemy = enemyObject.GetComponent<Enemy>();
			enemy.speed = enemySpeed[_currentWave];
		}
		_enemiesAlive += numEnemies;
		_timeLastWave = Time.time;
	}

	public string Serialize()
	{
		JSONObject jsonObject = new JSONObject ();
		jsonObject.Add ("waveTime", waveTime);

		JSONArray wavesArray = new JSONArray ();
		foreach (int wave in waves) {
			wavesArray.Add(wave);
		}

		JSONArray enemiesArray = new JSONArray ();
		foreach (int enemy in enemies) {
			enemiesArray.Add(enemy);
		}

		JSONArray strengthArray = new JSONArray ();
		foreach (int strength in enemyStrength) {
			strengthArray.Add(strength);
		}

		JSONArray speedArray = new JSONArray ();
		foreach (int speed in enemySpeed) {
			speedArray.Add(speed);
		}

		jsonObject.Add ("waves", wavesArray);
		jsonObject.Add ("enemies", enemiesArray);
		jsonObject.Add ("enemyStrength", strengthArray);
		jsonObject.Add ("enemySpeed", speedArray);

		return jsonObject.ToString ();
	}

	public void Deserialize(string serialized)
	{
		JSONObject jsonObject = JSONObject.Parse (serialized);

		JSONArray wavesArray = jsonObject.GetArray ("waves");
		// TODO ....
	}
}
