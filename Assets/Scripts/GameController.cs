﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class GameController : MonoBehaviour
{
	public NavMeshAgent player;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MoveAgentToPosition(Vector3 position)
	{
		player.SetDestination (position);
	}

	public void OnTerrainClick(BaseEventData eventData)
	{
		PointerEventData pointerData = eventData as PointerEventData;
		Vector3 position = pointerData.worldPosition;
		MoveAgentToPosition (position);
	}
}
