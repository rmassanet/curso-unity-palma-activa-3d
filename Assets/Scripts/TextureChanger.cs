﻿using UnityEngine;
using System.Collections;

public class TextureChanger : MonoBehaviour {

	public Material adMaterial;

	// Use this for initialization
	void Start () {
		Advertisement.OnTextureReady += TextureHandler;
	}

	void OnDestroy()
	{
		Advertisement.OnTextureReady -= TextureHandler;
	}
	
	void TextureHandler(Texture2D texture)
	{
		adMaterial.SetTexture ("_MainTex", texture);
	}
}
