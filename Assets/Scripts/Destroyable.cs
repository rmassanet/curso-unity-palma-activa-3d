﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Destroyable : MonoBehaviour {

	public delegate void DestroyedHandler();
	public static event DestroyedHandler OnDestroyed;

	public int cubesPerAxis;
	public float spacing;
	public float expansionForce;
	public GameObject explosionPrefab;
	public GameObject cubePrefab;
//	public GameObject destroyedPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Bullet") {
			DoDestroy();
		}
	}

	void DoDestroy()
	{
		if (OnDestroyed != null) {
			OnDestroyed();
		}

		GameObject.Instantiate (explosionPrefab, transform.position, Quaternion.identity);

		Vector3 parentPos = transform.position;
		float xPos = -spacing * cubesPerAxis * 0.5f;
		for(int x = 0; x < cubesPerAxis; x++)
		{
			float yPos = -spacing * cubesPerAxis * 0.5f;
			for(int y = 0; y < cubesPerAxis; y++)
			{
				float zPos = -spacing * cubesPerAxis * 0.5f;
				for(int z = 0; z < cubesPerAxis; z++)
				{
					GameObject newCube = (GameObject) GameObject.Instantiate(cubePrefab, new Vector3(parentPos.x + xPos, parentPos.y + yPos, parentPos.z + zPos), Quaternion.identity);
					newCube.rigidbody.AddForce(new Vector3(xPos, yPos, zPos) * expansionForce);
//					cubes[k++] = newCube;
					zPos += spacing;
				}
				yPos += spacing;
			}
			xPos += spacing;
		}

		Destroy (gameObject);
	}
}
