﻿using UnityEngine;
using System.Collections;

public class FreeCamera : MonoBehaviour
{
	public float rotationSensitivity;
	public float movementSpeed;

	private Vector3 _mouseDelta;
	private Vector3 _previousMousePosition;
	private Transform _transform;

	void Start ()
	{
		_previousMousePosition = Input.mousePosition;
		_transform = transform;
	}
	
	void Update ()
	{
		// Mouse look rotation
		Vector3 mousePosition = Input.mousePosition;
		_mouseDelta = mousePosition - _previousMousePosition;
		_previousMousePosition = mousePosition;

		float xRotation = rotationSensitivity * _mouseDelta.x;
		float yRotation = rotationSensitivity * _mouseDelta.y;
		Vector3 localRotation = _transform.localEulerAngles;
		localRotation.y += xRotation;
		localRotation.x -= yRotation;
		_transform.localEulerAngles = localRotation;

		// Movement
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		_transform.localPosition += _transform.right * h * movementSpeed * Time.deltaTime;
		_transform.localPosition += _transform.forward * v * movementSpeed * Time.deltaTime;
	}
}
