﻿using UnityEngine;
using System.Collections;

public class TestNetworkingPlayerSpawner : MonoBehaviour
{
	public GameObject playerPrefab;
	public Transform[] spawnPoints;

	public TestNetworkUIController uiController;

	private void Start()
	{
		Random.seed = (int)System.DateTime.Now.Ticks;
	}

	private void OnServerInitialized()
	{
		SpawnPlayer();
	}

	private void OnConnectedToServer()
	{
		SpawnPlayer();
	}

	private void SpawnPlayer()
	{
		uiController.DebugMessage("");
		int idx = Random.Range(0, spawnPoints.Length);
		Transform spawnPoint = spawnPoints[idx];
		GameObject newPlayer = (GameObject)Network.Instantiate(playerPrefab, spawnPoint.position, Quaternion.identity, 0);
	}
}
